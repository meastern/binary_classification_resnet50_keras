"""Define the model."""

from keras.applications.resnet50 import ResNet50
from keras.layers import Input
from keras.layers import Dropout, Flatten, Dense, GlobalAveragePooling2D, BatchNormalization
import keras.callbacks
import keras.optimizers
import datetime
from keras.models import Model


def callback_fn(log_dir_root, params):

    early_stopping = keras.callbacks.EarlyStopping(monitor='loss',
                                                   min_delta=0,
                                                   patience=params.patience,
                                                   verbose=1,
                                                   mode='auto')

    # TensorBoard callback
    now = datetime.datetime.utcnow().strftime("%Y%m%d%H%M%S")
    log_dir = "{}/run-{}/".format(log_dir_root, now)
    tensorboard = keras.callbacks.TensorBoard(log_dir=log_dir,
                                              write_graph=True,
                                              write_images=True)
    
    checkpoint = keras.callbacks.ModelCheckpoint(params.checkpoint_file,
                                                 monitor='val_acc',
                                                 verbose=1,
                                                 save_best_only=True,
                                                 mode='max')

    callbacks = [early_stopping, tensorboard, checkpoint]
    return callbacks

def model_fn_base(params):
    
    input_tensor = Input(shape=(params.image_size, params.image_size, 3))
    base_model=ResNet50(include_top=False,
                   weights='imagenet',
                   input_tensor=input_tensor,
                   input_shape=(params.image_size, params.image_size, 3))
    
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    # let's add a fully-connected layer
    x = Dense(1024, activation='relu')(x)
    x = Dropout(params.dropout, name='Dense_dropout')(x)
    # and a logistic layer 
    predictions = Dense(1, activation='sigmoid', name='fcout')(x)

    # this is the model we will train
    new_model = Model(inputs=base_model.input, outputs=predictions)
    
    for layer in base_model.layers:
        layer.trainable = False
    return new_model


def model_fn(params):

    new_model = model_fn_base(params)

    new_model.compile(loss='binary_crossentropy',
                  optimizer=keras.optimizers.adam(lr=params.learning_rate),
                  metrics=['accuracy'])
    new_model.summary()

    return new_model