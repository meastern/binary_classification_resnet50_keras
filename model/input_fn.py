"""Create the input data for Keras"""
from keras.preprocessing.image import ImageDataGenerator


def input_fn(image_dir, mode = 'train', batch_size = 32, target_size =(256, 256),
             class_mode = 'binary', rescale = 1, **kwargs ):
    
    if mode == 'train':
        datagen = ImageDataGenerator(rescale=rescale, **kwargs)
    elif mode == 'test':
        datagen = ImageDataGenerator(rescale=rescale)
    else:
        raise ValueError('mode value error.'
                         ' It should be either ''train'' or ''test'' ')

    data_generator = datagen.flow_from_directory(
                    image_dir,
                    target_size=target_size,
                    batch_size=batch_size,
                    class_mode=class_mode)
    return data_generator
